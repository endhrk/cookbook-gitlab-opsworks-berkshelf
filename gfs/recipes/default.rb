#
# Cookbook Name:: gfs_client
# Recipe:: default
#

gfs = node['gfs_client']

package "glusterfs-client"

directory gfs['mount_point'] do
  owner "root"
  group "root"
  mode 00777
  action :create
end

mount gfs['mount_point'] do
  device gfs['primary']
  fstype "glusterfs"
  options "defaults,_netdev,backupvolfile-server=gfs['secondary']"
  action [:mount, :enable]
end

